import 'package:flutter/material.dart';
import 'package:image_your_reddit/ui/screens/homeScreen.dart';
import 'package:image_your_reddit/ui/styling/colors.dart';

void main() async {
  runApp(MyApp());
}



class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Image Your Reddit',
        theme: ThemeData(
            backgroundColor: ColorPalette.Purple,
            primaryColor: ColorPalette.Orange,
            primaryColorLight: ColorPalette.White,
            accentColor: ColorPalette.Cyan,
            primaryColorDark: ColorPalette.Black),
        home: HomeScreen());
  }
}