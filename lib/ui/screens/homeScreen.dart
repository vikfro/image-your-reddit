import 'package:flutter/material.dart';
import 'package:image_your_reddit/data/model/post.dart';
import 'package:image_your_reddit/ui/widgets/post/postWidgets.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen> {
  List<Post> _posts;

  @override
  void initState() {
    // During development, if you change this code,
    // you will need to do a full restart instead of just a hot reload

    // You can't use async/await here,
    // We can't mark this method as async because of the @override
    fetchHomepageHotPosts().then((result) {
      // If we need to rebuild the widget with the resulting data,
      // make sure to use `setState`
      setState(() {
        _posts = result;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_posts == null || _posts.length == 0) {
      return Center(child: Container(
          height: 50,
          width: 50,
          padding: const EdgeInsets.all(20),
          child: const CircularProgressIndicator())
      );
    }

    final theme = Theme.of(context);

    final borderSide = BorderSide(width: 1, color: Colors.black26);

    return new Scaffold(
      backgroundColor: theme.backgroundColor,
        appBar: AppBar(
          backgroundColor: theme.backgroundColor,
            title: Container(
                decoration:
                    BoxDecoration(color: theme.backgroundColor),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Image ', style: TextStyle(color: theme.accentColor)),
                      Text('Your ', style: TextStyle(color: theme.primaryColorLight)),
                      Text('Reddit', style: TextStyle(color: theme.primaryColor))
                    ])
            )),
        body: Container(
            padding: const EdgeInsets.all(8),
            child: ListView(children: [
              PostList(buildList(0, _posts.length ~/ 2)),
                  Container(margin: EdgeInsets.symmetric(vertical: 20.0),
                      padding: EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                        border: Border(bottom: borderSide)
                          ),
                  child: Center(child: Text("Mocked subreddit",
                      style: TextStyle(color: theme.primaryColor, fontSize: 20.0),))),
              PostList(buildList(_posts.length ~/ 2, _posts.length))
            ])));
  }

  List<PostWidget> buildList(int begin, int end) {
    return _posts
                .sublist(begin, end)
                .map((post) => PostWidget(post))
                .toList();
  }
}
