import 'package:flutter/material.dart';
import 'package:image_your_reddit/data/model/post.dart';
import 'package:image_your_reddit/ui/widgets/post/postWidgets.dart';

class PostViewScreen extends StatelessWidget {
  final Post post;

  PostViewScreen(this.post);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Scaffold(
        body: Container(
            decoration: BoxDecoration(color: theme.backgroundColor),
            child: Stack(children: [
              ListView(
                  children: [PostTitleTextWidget(text: (post.title))]),
              BackButtonWidget()
            ])));
  }
}

class BackButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Positioned(
        bottom: 20,
        right: 20,
        child: ButtonTheme(
            child: FloatingActionButton(
          heroTag: "back",
          backgroundColor: theme.primaryColor,
          child: Icon(Icons.arrow_back, color: theme.primaryColorLight),
          onPressed: () => Navigator.pop(context),
        )));
  }
}
