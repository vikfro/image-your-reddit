import 'package:flutter/material.dart';
import 'package:image_your_reddit/data/model/comment.dart';

class CommentWidget extends StatelessWidget {
  final Author author;
  final Comment comment;

  CommentWidget(this.author, this.comment);
  
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Container(
      padding: EdgeInsets.all(8.0),
      margin: EdgeInsets.all(16.0),
        decoration: BoxDecoration(
          color: theme.backgroundColor,
          boxShadow: [BoxShadow(offset: Offset(0.0, 4.0), color: Colors.black38, blurRadius: 12)]),
        child:
        Column(
          children: [CommentAuthorWidget(author), CommentBodyWidget(comment.body)]
        )
    );
  }
}

class CommentBodyWidget extends StatelessWidget {
  final String body;

  CommentBodyWidget(this.body);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return  Container(
        padding: EdgeInsets.all(8.0),
        child: Text(body,
        style: TextStyle(color: theme.primaryColorLight))
      );
  }

}

class CommentAuthorWidget extends StatelessWidget {
  final Author author;

  CommentAuthorWidget(this.author);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Container(
        child: Text("u/${author.username}",
    style: TextStyle(color: theme.accentColor)));
  }
}