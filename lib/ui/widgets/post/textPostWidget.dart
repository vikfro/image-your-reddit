import 'package:flutter/cupertino.dart';
import 'package:image_your_reddit/data/model/post.dart';
import 'package:image_your_reddit/ui/widgets/comment/commentWidgets.dart';
import 'package:image_your_reddit/ui/widgets/post/postWidgets.dart';

class TextPostWidget extends StatelessWidget {
  final Post post;

  TextPostWidget(this.post);

  @override
  Widget build(BuildContext context) {
    return PostContainer(post,
        child: Column(children: [
          PostTitleWidget(post.title),
          Container(
              height: POST_HEIGHT - POST_TITLE_HEIGHT,
              padding: EdgeInsets.only(top: 20.0),
              child: ListView(
                  children: post.commentsPreview
                      .map((comment) => CommentWidget(comment.author, comment))
                      .toList()))
        ]));
  }
}
