import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_your_reddit/data/model/post.dart';
import 'package:image_your_reddit/ui/widgets/post/postWidgets.dart';

class ImagePostWidget extends StatelessWidget {
  final Post post;

  ImagePostWidget(this.post);

  @override
  Widget build(BuildContext context) {
    return PostContainer(post,
        child: Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
          Container(
              decoration: new BoxDecoration(color: Colors.black),
              child: new GradientImageWidget(post))
        ]));
  }
}

class GradientImageWidget extends StatelessWidget {
  final Post post;

  const GradientImageWidget(this.post);


  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Container(
        height: POST_HEIGHT,
        decoration: BoxDecoration(
          color: Colors.transparent,
          image: DecorationImage(
            fit: BoxFit.cover,
            image: NetworkImage(post.thumbnail.toString()),
          ),
        ),
      ),
      Container(
        height: POST_HEIGHT,
        decoration: BoxDecoration(
            color: Colors.white,
            gradient: LinearGradient(
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
                colors: [
                  Colors.black,
                  Colors.grey.withOpacity(0.0),
                ],
                stops: [
                  0.0,
                  1.0
                ])),
      ),
      Center(child: PostTitleTextWidget(text: post.title))
    ]);
  }
}