import 'package:flutter/material.dart';

class ColorPalette {
  static const Purple = Color.fromRGBO(64, 61, 88, 1);
  static const White = Color.fromRGBO(242, 239, 232, 1);
  static const Orange = Color.fromRGBO(252, 119, 83, 1);
  static const Cyan = Color.fromRGBO(102, 215, 209, 1);
  static const Black = Colors.black26;
}