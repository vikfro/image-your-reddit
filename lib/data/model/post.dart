import 'package:draw/draw.dart' as DRAW;
import 'package:image_your_reddit/config.dart';
import 'package:image_your_reddit/data/model/comment.dart';

class Post {
  final String title, author;
  final Uri thumbnail;
  final List<Comment> commentsPreview;

  Post(this.title, this.thumbnail, this.author, this.commentsPreview);

  factory Post.fromJson(DRAW.Submission submission, DRAW.CommentForest commentForest) {
    Uri thumbnail;
    if (submission.preview.length > 0) {
      thumbnail = submission.preview[0].source.url;
    }

    final commentForestList = commentForest.toList();

    List<Comment> comments = [];

    for (var i = 0; i < 10; i++) {
      final comment = commentForestList[i];

      if (!(comment is DRAW.MoreComments)) {
        comments.add(Comment.fromDRAW(comment));
      }
    }

    return Post(submission.title, thumbnail, submission.author, comments);
  }
}

Future<List<Post>> fetchHomepageHotPosts() async {
  print("Creating reddit instance");
  // Create the `Reddit` instance
  DRAW.Reddit reddit = await DRAW.Reddit.createUntrustedReadOnlyInstance(
    clientId: Config.clientId,
    deviceId: Config.deviceId,
    userAgent: Config.userAgent
  );

  print("Fetching first page");

  List<Post> frontFirstPagePosts = [];
  await for (final hotSubmission in reddit.front.hot(params: {'limit': '10'})) {
    final comments = await (hotSubmission as DRAW.Submission).refreshComments();
    frontFirstPagePosts.add(Post.fromJson(hotSubmission, comments));
  }

  print("fetched and parsed");
  return frontFirstPagePosts;
}