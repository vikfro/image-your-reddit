import 'package:draw/draw.dart' as DRAW;

class Comment {
  Author author;
  String body;
  int score;
  bool scoreHidden;
  bool removed;

  Comment(this.author, this.body, this.score, this.scoreHidden, this.removed);

  static fromDRAW(DRAW.Comment comment) {
    return Comment(Author.fromDRAW(), comment.body, comment.score, comment.scoreHidden, comment.removed);
  }
}

class Author {
  String username;

  Author(this.username);

  static fromDRAW() {
    return Author("Mocked author"); // TODO: DRAW lib has removed its Comment.author getter :'(
  }
}